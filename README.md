# README #

Android demo project to showcase the abilities of Mode-View-ViewModel (MVVM) Architecture.
The project also utilizes Google's Architectural Components libraries that help develop lifecycle aware components with ease. 

### What is this repository for? ###

This repository is useful for anyone who would like to learn, understand, and code the MVVM Architecture in Android.

### How do I get set up? ###

Simply download the project, and open it in Android Studio.

### Contribution guidelines ###

For anyone who would like to contribute to the project, kindly use the following guidelines:
> Always work on the dev branch, and make pull requests if any code needs to be updated.
> If the commit contains a feature, prefix "feat:" to your commit message.
> If the commit contains a fix, prefix "fix:" t your commit message.

### Who do I talk to? ###

Feel free to mail me at cyril.noah1@gmail.com