package com.cyril.mvvm.techtalk.example.data.local;

/**
 * Contains the entry constants for each of the tables in the local database.
 */
public class DataContract {
    private static final String TAG = DataContract.class.getSimpleName();

    /**
     * Database name.
     */
    public static final String DATABASE_NAME = "employeedb";

    /**
     * Employee table name.
     */
    public static final String EMPLOYEE_TABLE_NAME = "employee";

    /**
     * Contains all the entry constants related to the Employee table.
     */
    public static final class EmployeeEntry{
        public static final String COLUMN_EMPLOYEE_NAME = "employee_name";
    }
}
