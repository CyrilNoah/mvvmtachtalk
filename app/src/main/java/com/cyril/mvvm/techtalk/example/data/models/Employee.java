package com.cyril.mvvm.techtalk.example.data.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.cyril.mvvm.techtalk.example.data.local.DataContract;

/**
 * Model for containing the employee details
 */
@Entity(tableName = DataContract.EMPLOYEE_TABLE_NAME)
public class Employee {

    @PrimaryKey(autoGenerate = true)
    private int _id;

    @ColumnInfo(name = DataContract.EmployeeEntry.COLUMN_EMPLOYEE_NAME)
    private String employeeName;

    public Employee(String employeeName) {
        this.employeeName = employeeName;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }
}
