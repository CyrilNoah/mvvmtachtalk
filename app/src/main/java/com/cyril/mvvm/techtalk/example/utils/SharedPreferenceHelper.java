package com.cyril.mvvm.techtalk.example.utils;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.cyril.mvvm.techtalk.example.Application;

/**
 * Helper class to handle all the operations related to the {@link android.content.SharedPreferences}.
 */
public final class SharedPreferenceHelper {
    private static final String TAG = SharedPreferenceHelper.class.getSimpleName();

    private final String EMPTY_STRING = "";

    private SharedPreferences mSharedPreferences;
    private static SharedPreferenceHelper sInstance;

    private SharedPreferenceHelper() {

        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(
                Application.getInstance().getApplicationContext()
        );
    }

    /**
     * Method to provide a singleton instance of {@link SharedPreferenceHelper}.
     *
     * @return Singleton instance of the class.
     */
    public static SharedPreferenceHelper getInstance() {

        if (sInstance == null) {

            // Performing double-checked-locking.
            synchronized (SharedPreferenceHelper.class) {

                if (sInstance == null) {

                    sInstance = new SharedPreferenceHelper();

                }
            }
        }

        return sInstance;
    }

    /**
     * Method to store a {@link String} key-value pair in the {@link SharedPreferences}.
     *
     * @param key   String key with which the value is identified.
     * @param value String value that is to be stored in the SharedPreferences.
     */
    public void putString(String key, String value) {

        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    /**
     * Method to get the required {@link String} from the {@link SharedPreferences}.
     *
     * @param key String key to retrieve the String value.
     * @return Value retrieved from the SharedPreferences.
     */
    public String getString(String key) {
        return mSharedPreferences.getString(key, EMPTY_STRING);
    }

    /**
     * Method to clear all the key-value pairs for the SharedPreferences
     * associated to this application.
     */
    public void clearSharedPreferences() {

        mSharedPreferences.edit().clear().apply();
    }
}
