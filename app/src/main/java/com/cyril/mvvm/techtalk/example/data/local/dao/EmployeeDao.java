package com.cyril.mvvm.techtalk.example.data.local.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.cyril.mvvm.techtalk.example.data.local.DataContract;
import com.cyril.mvvm.techtalk.example.data.models.Employee;

import java.util.List;

/**
 * Data Access Object to deal with all the Employee related database operations.
 */
@Dao
public interface EmployeeDao {

    /**
     * Provides the list of Employee details from the table.
     *
     * @return List of Employees.
     */
    @Query("SELECT * FROM " + DataContract.EMPLOYEE_TABLE_NAME)
    List<Employee> getAllEmployees();

    /**
     * Inserts new Employee details into the table.
     *
     * @param employee Employee details.
     * @return A long value to address the success or failure of insertion operation.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE /*Replaces data if details already exist.*/)
    long insertEmployee(Employee employee);

    /**
     * Deletes an Employee entry from the table.
     *
     * @param employee Employee details.
     * @return A long value specifying the number of rows deleted.
     */
    @Delete
    int deleteEmployee(Employee employee);
}
