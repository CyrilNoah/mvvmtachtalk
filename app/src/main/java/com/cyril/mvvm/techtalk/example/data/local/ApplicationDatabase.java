package com.cyril.mvvm.techtalk.example.data.local;

import android.arch.persistence.db.SupportSQLiteOpenHelper;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.DatabaseConfiguration;
import android.arch.persistence.room.InvalidationTracker;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;

import com.cyril.mvvm.techtalk.example.Application;
import com.cyril.mvvm.techtalk.example.data.local.dao.EmployeeDao;
import com.cyril.mvvm.techtalk.example.data.models.Employee;

/**
 * Application level database access class which exposes
 * the {@link android.arch.persistence.room.RoomDatabase} database.
 */
@Database(entities = {Employee.class}, version = 1, exportSchema = false)
public abstract class ApplicationDatabase extends RoomDatabase {
    private static final String TAG = ApplicationDatabase.class.getSimpleName();

    /**
     * Singleton instance.
     */
    private volatile static ApplicationDatabase sInstance;

    /**
     * Provides a singleton instance.
     *
     * @return {@link ApplicationDatabase} instance.
     */
    public static ApplicationDatabase getInstance() {

        if (sInstance == null) {

            // Performing double-checked-locking to
            // assure thread-safe operations.
            synchronized (ApplicationDatabase.class) {

                if (sInstance == null) {

                    sInstance = Room.databaseBuilder(
                            Application.getInstance().getApplicationContext(),
                            ApplicationDatabase.class,
                            DataContract.DATABASE_NAME
                    ).build();

                }
            }
        }

        return sInstance;
    }

    /**
     * Provides Employee Dao to perform database operations
     * on Employee table.
     *
     * @return Employee Data Access Object.
     */
    public abstract EmployeeDao employeeDao();

    @Override
    protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration config) {
        return null;
    }

    @Override
    protected InvalidationTracker createInvalidationTracker() {
        return null;
    }
}
