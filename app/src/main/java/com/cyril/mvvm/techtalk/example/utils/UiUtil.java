package com.cyril.mvvm.techtalk.example.utils;

import android.content.Context;
import android.view.Gravity;
import android.widget.Toast;

/**
 * Util class to handle any UI related operations.
 */
public class UiUtil {

    /**
     * Displays a Toast message at the center of the screen.
     *
     * @param context Context of the current UI.
     * @param message Message to be displayed.
     */
    public void displayToast(Context context, String message) {

        Toast toast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, /*X-Offset*/ 0, /*Y-Offset*/0);
        toast.show();
    }
}
