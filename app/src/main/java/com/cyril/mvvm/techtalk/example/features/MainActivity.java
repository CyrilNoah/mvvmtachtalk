package com.cyril.mvvm.techtalk.example.features;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.databinding.Observable;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.widget.LinearLayout;

import com.cyril.mvvm.techtalk.example.R;
import com.cyril.mvvm.techtalk.example.adapters.EmployeeListAdapter;
import com.cyril.mvvm.techtalk.example.data.models.Employee;
import com.cyril.mvvm.techtalk.example.databinding.ActivityMainBinding;
import com.cyril.mvvm.techtalk.example.features.employee.EmployeeViewModel;
import com.cyril.mvvm.techtalk.example.utils.UiUtil;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();

    /**
     * Callback to notify and display {@link Snackbar} messages.
     */
    private Observable.OnPropertyChangedCallback snackbarMessageCallback;

    /**
     * View binding Object for the MainActivity.
     */
    private ActivityMainBinding mBinding;

    private EmployeeViewModel mEmployeeViewModel;

    private UiUtil mUiUtil;

    private EmployeeListAdapter mEmployeeListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUiUtil = new UiUtil();
        // Inflating the MainActivity View.
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        // Initializing Employee ViewModel.
        mEmployeeViewModel = ViewModelProviders.of(this).get(EmployeeViewModel.class);
        mBinding.setViewModel(mEmployeeViewModel);

        // Handles the displaying of Toast messages.
        handleSnackbarMessage();
        // Setting up the Employee list RecyclerView.
        setupEmployeeList();
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Registering the callbacks to handle immediate changes
        // in the Observable Objects.
        mEmployeeViewModel.message.addOnPropertyChangedCallback(snackbarMessageCallback);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Un-registering the callbacks.
        mEmployeeViewModel.message.removeOnPropertyChangedCallback(snackbarMessageCallback);
    }

    /**
     * Sets up the Employee list RecycleView.
     */
    private void setupEmployeeList() {
        final RecyclerView recyclerView = mBinding.rvMainActvEmployeeList;

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayout.VERTICAL));
        /*
          Using LiveData from Google's Architecture components library.
          The LiveData instance survives the configuration changes amd persists the data being held.
         */
        // Querying employee list, and setting it to the RecyclerView.
        mEmployeeViewModel.queryEmployeeList();
        mEmployeeViewModel.employeeListLiveData.observe(this, (employeeListObserver) -> {

            if (employeeListObserver != null) {

                if (mEmployeeListAdapter == null) {

                    mEmployeeListAdapter = new EmployeeListAdapter(employeeListObserver);
                    recyclerView.setAdapter(mEmployeeListAdapter);

                } else {

                    mEmployeeListAdapter.swapEmployeeList(employeeListObserver);
                }
            }
        });

        // Setting ItemTouchHelper to handle deletion of items on swipe.
        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0,
                ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

            @Override
            public boolean onMove(RecyclerView recyclerView,
                                  RecyclerView.ViewHolder viewHolder,
                                  RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                String id = viewHolder.itemView.getTag().toString();
                // Deleting the Employee details with the provided id
                // from the database.
                Employee employee = mEmployeeViewModel.employeeListLiveData.getValue().get(
                        Integer.parseInt(id)
                );

                if (employee != null) mEmployeeViewModel.deleteEmployeeData(employee);
            }
        }).attachToRecyclerView(recyclerView);
    }

    /**
     * Handles the display of {@link Snackbar} messages.
     */
    private void handleSnackbarMessage() {

        snackbarMessageCallback = new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable sender, int propertyId) {
                // Displaying the Toast message whenever a change is observed
                // in the message Object.
                Snackbar.make(
                        mBinding.clMainActvRoot,
                        mEmployeeViewModel.message.get(),
                        Snackbar.LENGTH_SHORT
                ).show();
            }
        };
    }
}
