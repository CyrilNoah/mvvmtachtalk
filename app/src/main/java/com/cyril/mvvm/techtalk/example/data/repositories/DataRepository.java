package com.cyril.mvvm.techtalk.example.data.repositories;

import android.os.AsyncTask;

import com.cyril.mvvm.techtalk.example.data.local.ApplicationDatabase;
import com.cyril.mvvm.techtalk.example.data.models.Employee;

import java.util.List;

/**
 * Repository to perform raw database operations
 * and provide data to the ViewModels.
 */
public class DataRepository {
    private static final String TAG = DataRepository.class.getSimpleName();

    private static DataRepository sInstance;
    private ApplicationDatabase mDatabase;

    private static final String INSERTION_FAILURE_MESSAGE = "Failed to insert data";
    private static final String INSERTION_SUCCESS_MESSAGE = "Data insertion successful";
    private static final String DELETION_FAILURE_MESSAGE = "Failed to delete data";
    private static final String DELETION_SUCCESS_MESSAGE = "Data deletion successful";
    private static final String QUERY_FAILURE_MESSAGE = "Failed to retrieve the data.";

    private DataRepository() {
        mDatabase = ApplicationDatabase.getInstance();
    }

    /**
     * Method to provide singleton instance of {@link DataRepository}.
     *
     * @return Singleton instance of the class.
     */
    public static DataRepository getInstance() {

        if (sInstance == null) {

            // Performing double-checked-locking.
            synchronized (DataRepository.class) {

                if (sInstance == null) {

                    sInstance = new DataRepository();

                }
            }

        }

        return sInstance;
    }

    /**
     * Inserts Employee data into the Employee table.
     *
     * @param employee  Employee details.
     * @param callbacks {@link InsertEmployeeDataCallbacks} to address success or failure.
     */
    public void insertEmployeeData(Employee employee,
                                   final InsertEmployeeDataCallbacks callbacks) {
        new InsertAsyncTask(callbacks).execute(employee);
    }

    /**
     * Deletes an existing Employee data entry from the Employee table.
     *
     * @param employee  Employee details.
     * @param callbacks {@link DeleteEmployeeDataCallbacks} to address success or failure.
     */
    public void deleteEmployeeData(Employee employee,
                                   final DeleteEmployeeDataCallbacks callbacks) {
        new DeleteAsyncTask(callbacks).execute(employee);
    }

    /**
     * Queries a list of all the Employee entries from the Employee table.
     *
     * @param callbacks {@link QueryEmployeeDataCallbacks} to address success or failure.
     */
    public void queryEmployeeData(final QueryEmployeeDataCallbacks callbacks) {
        new QueryAsyncTask(callbacks).execute();
    }


    public interface InsertEmployeeDataCallbacks {
        void onSuccess(String successMessage);

        void onFailure(String errorMessage);
    }

    public interface DeleteEmployeeDataCallbacks {
        void onSuccess(String successMessage);

        void onFailure(String errorMessage);
    }

    public interface QueryEmployeeDataCallbacks {
        void onSuccess(List<Employee> employees);

        void onFailure(String errorMessage);
    }

    /**
     * AsyncTask to insert Employee data.
     */
    private class InsertAsyncTask extends AsyncTask<Employee, Void, Long> {
        InsertEmployeeDataCallbacks callbacks;

        InsertAsyncTask(InsertEmployeeDataCallbacks callbacks) {
            this.callbacks = callbacks;
        }

        @Override
        protected Long doInBackground(Employee... employees) {
            return mDatabase.employeeDao().insertEmployee(employees[0]);
        }

        @Override
        protected void onPostExecute(Long rowsInserted) {
            super.onPostExecute(rowsInserted);
            if (rowsInserted > 0) {
                callbacks.onSuccess(INSERTION_SUCCESS_MESSAGE);
            } else {
                callbacks.onFailure(INSERTION_FAILURE_MESSAGE);
            }
        }
    }

    /**
     * AsyncTask to delete Employee data.
     */
    private class DeleteAsyncTask extends AsyncTask<Employee, Void, Integer> {
        DeleteEmployeeDataCallbacks callbacks;

        DeleteAsyncTask(DeleteEmployeeDataCallbacks callbacks) {
            this.callbacks = callbacks;
        }

        @Override
        protected Integer doInBackground(Employee... employees) {
            return mDatabase.employeeDao().deleteEmployee(employees[0]);
        }

        @Override
        protected void onPostExecute(Integer rowsDeleted) {
            super.onPostExecute(rowsDeleted);

            if (rowsDeleted > 0) {
                callbacks.onSuccess(DELETION_SUCCESS_MESSAGE);
            } else {
                callbacks.onFailure(DELETION_FAILURE_MESSAGE);
            }
        }
    }

    /**
     * AsyncTask to query Employee list.
     */
    private class QueryAsyncTask extends AsyncTask<Void, Void, List<Employee>> {
        QueryEmployeeDataCallbacks callbacks;

        QueryAsyncTask(QueryEmployeeDataCallbacks callbacks) {
            this.callbacks = callbacks;
        }

        @Override
        protected List<Employee> doInBackground(Void... voids) {
            return mDatabase.employeeDao().getAllEmployees();
        }

        @Override
        protected void onPostExecute(List<Employee> employees) {
            super.onPostExecute(employees);

            if (employees != null) {
                callbacks.onSuccess(employees);
            } else {
                callbacks.onFailure(QUERY_FAILURE_MESSAGE);
            }
        }
    }
}
