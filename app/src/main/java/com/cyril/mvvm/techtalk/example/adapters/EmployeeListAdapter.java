package com.cyril.mvvm.techtalk.example.adapters;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.cyril.mvvm.techtalk.example.R;
import com.cyril.mvvm.techtalk.example.data.models.Employee;
import com.cyril.mvvm.techtalk.example.databinding.ItemEmployeeDetailsBinding;

import java.util.List;

/**
 * RecyclerView Adapter for populating the Employee list.
 */
public class EmployeeListAdapter extends RecyclerView.Adapter<EmployeeListAdapter.ViewHolder> {
    private static final String TAG = EmployeeListAdapter.class.getSimpleName();

    private List<Employee> employees;

    public EmployeeListAdapter(List<Employee> employees) {
        this.employees = employees;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        ViewDataBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_employee_details, parent, false);

        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setBinding(employees.get(position));
        // Setting each item's tag as their respective position in
        // in the list.
        Integer id = position;
        holder.itemView.setTag(id);
    }

    /**
     * Swaps the existing list of Employees with a new list.
     *
     * @param employees Employee list.
     */
    public void swapEmployeeList(List<Employee> employees) {
        this.employees = employees;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return employees.size();
    }

    /**
     * ViewHolder to bind the data to each item's View.
     */
    public class ViewHolder extends RecyclerView.ViewHolder {
        ItemEmployeeDetailsBinding binding;

        public ViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());

            this.binding = (ItemEmployeeDetailsBinding) binding;
        }

        /**
         * Binds the Employee details to the View.
         *
         * @param employee Employee details.
         */
        public void setBinding(Employee employee) {
            binding.setEmployee(employee);
        }
    }
}
