package com.cyril.mvvm.techtalk.example;

/**
 * Application class for the project.
 */
public final class Application extends android.app.Application {
    private static final String TAG = Application.class.getSimpleName();

    /**
     * Static instance of the {@link Application} class which can be used to
     * access the Application context.
     */
    private static Application sInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
    }

    /**
     * Method to provide a singleton instance of the {@link Application} class.
     * </p>
     * HINT: This instance can be used to access Application context wherever required.
     *
     * @return Application class instance.
     */
    public static Application getInstance() {
        return sInstance;
    }
}
