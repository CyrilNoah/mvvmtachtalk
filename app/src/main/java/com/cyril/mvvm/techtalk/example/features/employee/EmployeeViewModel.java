package com.cyril.mvvm.techtalk.example.features.employee;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.databinding.ObservableField;

import com.cyril.mvvm.techtalk.example.Application;
import com.cyril.mvvm.techtalk.example.data.models.Employee;
import com.cyril.mvvm.techtalk.example.data.repositories.DataRepository;

import java.util.List;

/**
 * ViewModel class for the Employee related operations.
 */
public class EmployeeViewModel extends ViewModel {
    private static final String TAG = EmployeeViewModel.class.getSimpleName();

    private DataRepository mDataRepository;

    /**
     * Observable to persist Employee name.
     */
    public final ObservableField<String> employeeName = new ObservableField<>();

    /**
     * Observable to handle success or error messages.
     */
    public final ObservableField<String> message = new ObservableField<>();

    /**
     * {@link MutableLiveData} to persist Employee list.
     */
    public MutableLiveData<List<Employee>> employeeListLiveData = new MutableLiveData<>();

    /**
     * Observable to notify the completion of data fetching operations.
     */
    public final ObservableField<Boolean> isDataPending = new ObservableField<>();

    public EmployeeViewModel() {
        mDataRepository = DataRepository.getInstance();
    }

    /**
     * Inserts Employee into the database.
     */
    public void insertEmployeeData() {

        if (employeeName.get().isEmpty()) {
            message.set("Please enter the employee name");
        } else {

            mDataRepository.insertEmployeeData(new Employee(employeeName.get().trim()),
                    new DataRepository.InsertEmployeeDataCallbacks() {
                        @Override
                        public void onSuccess(String successMessage) {

                            employeeName.set("");
                            message.set(successMessage);
                            queryEmployeeList();
                        }

                        @Override
                        public void onFailure(String errorMessage) {

                            employeeName.set("");
                            message.set(errorMessage);
                        }
                    });
        }
    }

    /**
     * Deletes existing Employee entry from the database.
     */
    public void deleteEmployeeData(Employee employee) {

        mDataRepository.deleteEmployeeData(employee, new DataRepository.DeleteEmployeeDataCallbacks() {
            @Override
            public void onSuccess(String successMessage) {
                message.set(successMessage);
                queryEmployeeList();
            }

            @Override
            public void onFailure(String errorMessage) {
                message.set(errorMessage);
            }
        });
    }

    /**
     * Queries the total Employee list from the Database.
     */
    public void queryEmployeeList() {

        // This boolean value notifies the UI when the data fetching progress
        // had started, and when it had ended.
        isDataPending.set(true);

        mDataRepository.queryEmployeeData(new DataRepository.QueryEmployeeDataCallbacks() {
            @Override
            public void onSuccess(List<Employee> employees) {

                isDataPending.set(false);

                employeeListLiveData.setValue(employees);
            }

            @Override
            public void onFailure(String errorMessage) {

                isDataPending.set(false);

                message.set(errorMessage);
            }
        });
    }
}
